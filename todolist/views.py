from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ToDoItem
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

#  we import the build-in "User" model to be able to do operations to the build-in Users table from django.
# Create your views here.

def index(request):
    todoitem_list = ToDoItem.objects.all()

    template = loader.get_template('todolist/index.html')
    context = {
        'todoitem_list': todoitem_list,
        'user': request.user
    }

    return HttpResponse(template.render(context, request))
    # return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    
    todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    
    # model_to_dict function translate a data model into a python dictionary = object. this is so that we can use the data as a regular object/dictionary.
    
    return render(request, "todolist/todoitem.html", todoitem)

    print(todoitem)

def register(request):

    users = User.objects.all()
    is_user_registered = False
    

    #  loops through each existing user and checks if they already exist in the table
    for individual_user in users:
        if individual_user.username == "johndoe":
            is_user_registered = True
            break
    
    context = {
        "is_user_registered": is_user_registered
    }            

    if is_user_registered == False:
        # creating a new instance of the user model.
        user = User()
        # assign data values to each properties of the model
        user.username = "johndoe"
        user.first_name = "John"
        user.last_name = "Doe"
        user.email = "john@mail.com"
        user.set_password("john1234")
        user.is_staff = False
        user.is_active = True
        # save the new insatnce of a user into databse.
        user.save()

        # the context will be the data to be passed to the template
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name
        }

    return render(request, "todolist/register.html", context)

def change_password(request):
    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")

    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")

        authenticated_user.set_password("johndoe1")
        authenticated_user.save()

        is_user_authenticated = True

    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "todolist/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "johndoe1"

	user = authenticate(username=username, password=password)

	context = {
		"is_user_authenticated": False 
	}

	if user is not None:
		# The 'login' function saves the user's ID in Django's session. The request will be the one that handles the user's data and we can access the user by using 'request.user'.
		login(request, user)
		return redirect("index")
	else:
		return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")


